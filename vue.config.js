const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})
module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
        @import '~@/scss/abstracts/_media.scss';
        @import '~@/scss/abstracts/_variables.scss';
        `
      }
    }
  }
}
